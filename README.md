# 漆彩三晋项目

## 使用 [vue-cli-electron-packager](https://gitee.com/huisir001/vue-cli-electron-packager) 脚手架开发

## 注意
- 这里打包使用的是electron-packager
- electron-packager打包完成后包含app目录项目源码，和electron-Builder打包的结果不同
- package.json中的`set ELECTRON_DISABLE_SECURITY_WARNINGS=true`设置环境变量可以避免窗口console中出现黄色警告

## 打包生成的程序可搭配个人制作的[min_electron](https://gitee.com/huisir001/min_electron)使用
> 将打包生成程序的`resources`目录的app拷贝到`min_electron`的`resources`目录即可，具体查看	`min_electron`的介绍

## 在vue模版(.vue后缀文件)中使用node模块
1. 在public/index.html中添加`<script> var NODE = require </script>`(本例中已添加)
2. 在vue模板中使用NODE('[模块名]')来调用node模块，如：
```
<script>
    const $electron  = NODE('electron');
    const $remote = $electron.remote;
    const $path = NODE('path');
    const $fs = NODE('fs');
</script>
```

## Project setup
```
npm install
```

### 预览应用(已实现开发热更新)
```
npm run dev
```

### 打包应用
```
npm run build
```
### 作者
- Gitee：[HuiSir](https://gitee.com/huisir001/)
- Blog：[醉风云博客](http://www.zuifengyun.com/)
- QQ：273250950

>版权所有，翻版必究

