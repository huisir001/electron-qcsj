const {app, BrowserWindow,Menu} = require('electron')

// 将窗口创建为全局变量以便修改
let mainWindow

function createWindow () {
  //隐藏菜单栏
  Menu.setApplicationMenu(null)
  // Create the browser window.
  mainWindow = new BrowserWindow({
    fullscreen: true,
    // width: 1200,
    // height: 750,
    //resizable: false, //宽高固定
    frame: false, //边框隐藏
    webPreferences: {
      nodeIntegration: true,
      webSecurity: false  //关闭窗口跨域,可访问本地绝对路径资源(图片)
    }
  })
	
	// and load the index.html of the app.
	mainWindow.loadFile('./index.html')

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
module.exports = app
