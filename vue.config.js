/**
 *	@author HuiSir q273250950
 *  @git https://gitee.com/huisir001/vue-cli-electron-packager.git
 * 	版权所有,翻版必究
 */

const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    publicPath: './',	//避免路径为绝对路径
	devServer: {
		port:'2000'
	},
	chainWebpack: config => {
		//将package.json复制到dist目录下，因为打包electron需要
		config.plugin('copy').use(CopyWebpackPlugin, [[
			{
				from: 'package.json', 
				to:'./'
			},
			{
				from: 'main.js', 
				to:'./'
			},
			{
				from: 'public/favicon.ico', 
				to:'./'
			},
			{
				from: 'public/libs', 
				to:'./libs/'
			},
			{
				from: 'public/bgs',
				to:'./bgs/'
			},
		]]);
	}
}