/**
 * @author HuiSir q273250950
 * @git https://gitee.com/huisir001/
 * @blog http://www.zuifengyun.com/
 */

import Vue from 'vue'
import Router from 'vue-router'
// import ymyq from './views/ymyq.vue'
// import qtgy from './views/qtgy.vue'
import jptj from './views/jptj.vue'

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'ymyq',	//一木一器
    //   component: ymyq
    // }
		// {
		//   path: '/',
		//   name: 'qtgy',		//青铜古韵
		//   component: qtgy
		// }
		{
		  path: '/',
		  name: 'jptj',		//精品推荐
		  component: jptj
		}
  ]
})
